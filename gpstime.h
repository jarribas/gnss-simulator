//
// Created by javier on 3/11/15.
//

#ifndef GNSS_SIM_GPSTIME_H
#define GNSS_SIM_GPSTIME_H

class gpstime
{
public:
    int week;   /*!< GPS week number (since January 1980) */
    double sec; /*!< second inside the GPS \a week */

    double gps2jd(const int rollover);
    /*!
     * Default constructor
     */
    gpstime();
};


#endif  //GNSS_SIM_GPSTIME_H
