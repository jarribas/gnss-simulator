########################################################################
# Find VOLK (Vector-Optimized Library of Kernels)
########################################################################

set(FPHSA_NAME_MISMATCHED ON)
include(FindPkgConfig)
pkg_check_modules(PC_VOLK volk)

find_path(
    VOLK_INCLUDE_DIRS
    NAMES volk/volk.h
    HINTS $ENV{VOLK_DIR}/include
          ${PC_VOLK_INCLUDEDIR}
    PATHS /usr/local/include
          /usr/include
          ${GNURADIO_INSTALL_PREFIX}/include
)

find_library(
    VOLK_LIBRARIES
    NAMES volk
    HINTS $ENV{VOLK_DIR}/lib
          ${PC_VOLK_LIBDIR}
    PATHS /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          ${GNURADIO_INSTALL_PREFIX}/lib
)

set(VOLK_VERSION ${PC_VOLK_VERSION})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(VOLK DEFAULT_MSG VOLK_LIBRARIES VOLK_INCLUDE_DIRS)
mark_as_advanced(VOLK_LIBRARIES VOLK_INCLUDE_DIRS VOLK_VERSION)

if(VOLK_FOUND AND NOT TARGET Volk::volk)
    add_library(Volk::volk SHARED IMPORTED)
    set_target_properties(Volk::volk PROPERTIES
        IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
        IMPORTED_LOCATION "${VOLK_LIBRARIES}"
        INTERFACE_INCLUDE_DIRECTORIES "${VOLK_INCLUDE_DIRS}"
        INTERFACE_LINK_LIBRARIES "${VOLK_LIBRARIES}"
    )
endif()
