//
// Created by javier on 6/11/15.
//

#ifndef GNSS_SIM_SIMPLE_RINEX2_WRITER_H
#define GNSS_SIM_SIMPLE_RINEX2_WRITER_H

#include "GPS_L1_CA.h"
#include "datetime.h"
#include "gpstime.h"
#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>


class simple_rinex2_writer
{
public:
    bool write_obs_header(const std::string &fecha_rinex_str, std::vector<double> rx_pos, float interv, datetime date_time);
    bool write_obs_epoch(int nsats, int *prn, gpstime *gps_time, const double *dist_m, const double *acc_carrier_phase_cycles, double *acc_carrier_phase_l2_cycles, const double *doppler_l1);
    bool write_binary_epoch(int nsats, const int *prn, gpstime *gps_time, const double *dist_m, const double *true_dist_m, const double *acc_carrier_phase_cycles, double *acc_carrier_phase_l2_cycles, const double *doppler_l1);
    bool open_obs_file(const std::string &filename);
    bool open_dump_file(std::string out_file);
    bool d_dump;

private:
    FILE *obs_file_ptr;

    std::string d_dump_filename;
    std::ofstream d_dump_file;
};


#endif  //GNSS_SIM_SIMPLE_RINEX2_WRITER_H
