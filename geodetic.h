//
// Created by javier on 6/11/15.
//

#ifndef GNSS_SIM_GEODETIC_H
#define GNSS_SIM_GEODETIC_H

#endif  //GNSS_SIM_GEODETIC_H


/*! \brief Convert Earth-centered Earth-fixed (ECEF) into Lat/Long/Heighth
*  \param[in] xyz Input Array of X, Y and Z ECEF coordinates
*  \param[out] llh Output Array of Latitude, Longitude and Height
*/
void xyz2llh(const double *xyz, double *llh);

/*! \brief Convert Lat/Long/Height into Earth-centered Earth-fixed (ECEF)
 *  \param[in] llh Input Array of Latitude, Longitude and Height
 *  \param[out] xyz Output Array of X, Y and Z ECEF coordinates
 */
void llh2xyz(const double *llh, double *xyz);


/*! \brief Compute the intermediate matrix for LLH to ECEF
  *  \param[in] llh Input position in Latitude-Longitude-Height format
  *  \param[out] t Three-by-Three output matrix
  */
void ltcmat(const double *llh, double t[3][3]);


/*! \brief Convert Earth-centered Earth-Fixed to ?
*  \param[in] xyz Input position as vector in ECEF format
*  \param[in] t Intermediate matrix computed by \ref ltcmat
*  \param[out] neu Output position as North-East-Up format
*/
void ecef2neu(const double *xyz, double t[3][3], double *neu);


/*! \brief Convert North-Eeast-Up to Azimuth + Elevation
*  \param[in] neu Input position in North-East-Up format
*  \param[out] azel Output array of azimuth + elevation as double
*/
void neu2azel(double *azel, const double *neu);
