/*!
 * \file main.cc
 * \brief Main file of the GNSS-SIM program.
 * \author Javier Arribas, 2015. jarribas(at)cttc.es
 *
 * It sets up the logging system, creates a the main thread,
 * makes it run, and releases memory back when the main thread has ended.
 *
 * -------------------------------------------------------------------------
 *
 * Copyright (C) 2010-2015 (see AUTHORS file for a list of contributors)
 *
 * GNSS-SDR is a software defined Global Navigation
 * Satellite Systems Simulator
 *
 * This file is part of GNSS-SIM.
 *
 * GNSS-SIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNSS-SIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNSS-SIM. If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------
 */
#ifndef GNSS_SIM_VERSION
#define GNSS_SIM_VERSION "0.0.2"
#endif

#ifndef GOOGLE_STRIP_LOG
#define GOOGLE_STRIP_LOG 0
#endif
#include "simulator_control.h"
#include <boost/exception/diagnostic_information.hpp>
#include <boost/exception_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <limits>
#include <string>

#if CUDA_GPU_ACCEL
// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>
#endif

#if USE_GLOG_AND_GFLAGS
#include <gflags/gflags.h>  // for ShutDownCommandLineFlags
#include <glog/logging.h>
#if GFLAGS_OLD_NAMESPACE
namespace gflags
{
using namespace google;
}
#endif
using google::LogMessage;

DECLARE_string(log_dir);
DEFINE_string(obs_pos_file, "circle.csv", "Observer positions file, in .csv or .nmea format");
DEFINE_string(rinex_nav_file, "RINEX.NAV", "Satellite ephemeris file in RINEX 2.10 navigation format");
DEFINE_string(rinex_obs_file, "sim.15o", "RINEX 2.10 observation file output");
DEFINE_string(sig_out_file, "signal_out.bin", "Baseband signal output file. Will be stored in int8_t IQ multiplexed samples");
DEFINE_double(sampling_freq, 2.6e6, "Baseband sampling frequency [Hz]");
DEFINE_string(static_position, "", "Lat,Lon,Hgt,N_Points (static mode) e.g. 30.286502,120.032669,100,1000");
DEFINE_double(CN0_dBHz, std::numeric_limits<double>::infinity(), "Carrier-to-Noise ratio (CN0) [dB-Hz]");
#else
#include <absl/flags/flag.h>
#include <absl/flags/parse.h>
#include <absl/flags/usage.h>
#include <absl/flags/usage_config.h>
#include <absl/log/globals.h>
#include <absl/log/initialize.h>
#include <absl/log/log.h>
#include <absl/log/log_sink.h>
#include <absl/log/log_sink_registry.h>
#include <sys/stat.h>

ABSL_FLAG(std::string, log_dir, "", "Directory for log files");
ABSL_FLAG(std::string, obs_pos_file, "circle.csv", "Observer positions file, in .csv or .nmea format");
ABSL_FLAG(std::string, rinex_nav_file, "RINEX.NAV", "Satellite ephemeris file in RINEX 2.10 navigation format");
ABSL_FLAG(std::string, rinex_obs_file, "sim.15o", "RINEX 2.10 observation file output");
ABSL_FLAG(std::string, sig_out_file, "signal_out.bin", "Baseband signal output file. Will be stored in int8_t IQ multiplexed samples");
ABSL_FLAG(double, sampling_freq, 2.6e6, "Baseband sampling frequency [Hz]");
ABSL_FLAG(std::string, static_position, "", "Lat,Lon,Hgt,N_Points (static mode) e.g. 30.286502,120.032669,100,1000");
ABSL_FLAG(double, CN0_dBHz, std::numeric_limits<double>::infinity(), "Carrier-to-Noise ratio (CN0) [dB-Hz]");

std::string GnssSimVersionString() { return std::string("gnss-sdr version ") + std::string(GNSS_SIM_VERSION) + "\n"; }

static inline void GetTempDirectories(std::vector<std::string>& list)
{
    list.clear();
    // Directories, in order of preference. If we find a dir that
    // exists, we stop adding other less-preferred dirs
    const char* candidates[] = {
        // Non-null only during unittest/regtest
        std::getenv("TEST_TMPDIR"),

        // Explicitly-supplied temp dirs
        std::getenv("TMPDIR"),
        std::getenv("TMP"),

        // If all else fails
        "/tmp",
    };
    for (auto d : candidates)
        {
            if (!d) continue;  // Empty env var

            // Make sure we don't surprise anyone who's expecting a '/'
            std::string dstr = d;
            if (dstr[dstr.size() - 1] != '/')
                {
                    dstr += "/";
                }
            list.push_back(dstr);

            struct stat statbuf;
            if (!stat(d, &statbuf) && S_ISDIR(statbuf.st_mode))
                {
                    // We found a dir that exists - we're done.
                    return;
                }
        }
}


static inline void GetExistingTempDirectories(std::vector<std::string>& list)
{
    GetTempDirectories(list);
    auto i_dir = list.begin();
    while (i_dir != list.end())
        {
            if (access(i_dir->c_str(), 0))
                {
                    i_dir = list.erase(i_dir);
                }
            else
                {
                    ++i_dir;
                }
        };
}


static inline std::string GetTempDir()
{
    std::vector<std::string> temp_directories_list;
    GetExistingTempDirectories(temp_directories_list);

    if (temp_directories_list.empty())
        {
            std::cerr << "No temporary directory found\n";
            exit(EXIT_FAILURE);
        }

    // Use first directory from list of existing temporary directories.
    return temp_directories_list.front();
}

class GnssSimLogSink : public absl::LogSink
{
public:
    GnssSimLogSink()
    {
        if (!absl::GetFlag(FLAGS_log_dir).empty())
            {
                filename = absl::GetFlag(FLAGS_log_dir) + "/gnss-sim.log";
            }
        else
            {
                filename = GetTempDir() + "/gnss-sim.log";
            }
        logfile.open(filename);
    }
    void Send(const absl::LogEntry& entry) override
    {
        logfile << entry.text_message_with_prefix_and_newline() << std::flush;
    }

private:
    std::ofstream logfile;
    std::string filename;
};

#endif


int main(int argc, char** argv)
{
    const std::string intro_help(
        std::string("\nGNSS-SIM is an Open Source GNSS Software Defined Simulator\n") +
        "Copyright (C) 2015 (see AUTHORS file for a list of contributors)\n" +
        "This program comes with ABSOLUTELY NO WARRANTY;\n" +
        "See COPYING file to see a copy of the General Public License\n \n");

    const std::string gnss_sdr_version(GNSS_SIM_VERSION);

#if USE_GLOG_AND_GFLAGS
    google::SetUsageMessage(intro_help);
    google::SetVersionString(gnss_sdr_version);
    google::ParseCommandLineFlags(&argc, &argv, true);
    std::string log_dir_str(FLAGS_log_dir);
#else
    absl::FlagsUsageConfig empty_config;
    empty_config.version_string = &GnssSimVersionString;
    absl::SetFlagsUsageConfig(empty_config);
    absl::SetProgramUsageMessage(intro_help);
    absl::ParseCommandLine(argc, argv);
    std::string log_dir_str(absl::GetFlag(FLAGS_log_dir));
#endif

    std::cout << "Initializing GNSS-SIM v" << gnss_sdr_version << " ... Please wait." << std::endl;

#if CUDA_GPU_ACCEL
    // Reset the device
    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    cudaDeviceReset();
    std::cout << "Reset CUDA device done " << std::endl;
#endif

    if (GOOGLE_STRIP_LOG == 0)
        {
#if USE_GLOG_AND_GFLAGS
            google::InitGoogleLogging(argv[0]);
#else
            absl::LogSink* theSink = new GnssSimLogSink;
            absl::AddLogSink(theSink);
            absl::InitializeLog();
#endif
            if (log_dir_str.empty())
                {
                    std::cout << "Logging will be done at "
                              << boost::filesystem::temp_directory_path()
                              << std::endl
                              << "Use gnss-sdr --log_dir=/path/to/log to change that."
                              << std::endl;
                }
            else
                {
                    const boost::filesystem::path p(log_dir_str);
                    if (!boost::filesystem::exists(p))
                        {
                            std::cout << "The path "
                                      << log_dir_str
                                      << " does not exist, attempting to create it."
                                      << std::endl;
                            boost::system::error_code ec;
                            if (!boost::filesystem::create_directory(p, ec))
                                {
                                    std::cout << "Could not create the " << log_dir_str << " folder. GNSS-SDR program ended." << std::endl;
#if USE_GLOG_AND_GFLAGS
                                    google::ShutDownCommandLineFlags();
#endif
                                    std::exit(0);
                                }
                        }
                    std::cout << "Logging with be done at " << log_dir_str << std::endl;
                }
        }


    std::unique_ptr<simulator_control> sim_ctl(new simulator_control());

    // record startup time
    struct timeval tv
    {
    };
    gettimeofday(&tv, nullptr);
    long long int begin = tv.tv_sec * 1000000 + tv.tv_usec;
    try
        {
// READ command line parameters and initialize files
// Observer coordinates
#if USE_GLOG_AND_GFLAGS
            if (FLAGS_static_position.size() < 10)
#else
            if (absl::GetFlag(FLAGS_static_position).size() < 10)
#endif
                {
                    // dynamic mode
#if USE_GLOG_AND_GFLAGS
                    if (FLAGS_obs_pos_file.find(".csv") != std::string::npos)
                        {
                            if (sim_ctl->readUserMotion(FLAGS_obs_pos_file) == -1)
                                {
                                    std::cout << "Problem reading " << FLAGS_obs_pos_file << " file" << std::endl;
                                }
                        }
                    else if (FLAGS_obs_pos_file.find(".nmea") != std::string::npos)
                        {
                            if (sim_ctl->readNmeaGGA(FLAGS_obs_pos_file) == -1)
                                {
                                    std::cout << "Problem reading " << FLAGS_obs_pos_file << " file" << std::endl;
                                }
                        }
#else
                    if (absl::GetFlag(FLAGS_obs_pos_file).find(".csv") != std::string::npos)
                        {
                            std::string str = absl::GetFlag(FLAGS_obs_pos_file);
                            if (sim_ctl->readUserMotion(str) == -1)
                                {
                                    std::cout << "Problem reading " << absl::GetFlag(FLAGS_obs_pos_file) << " file" << std::endl;
                                }
                        }
                    else if (absl::GetFlag(FLAGS_obs_pos_file).find(".nmea") != std::string::npos)
                        {
                            std::string str = absl::GetFlag(FLAGS_obs_pos_file);
                            if (sim_ctl->readNmeaGGA(str) == -1)
                                {
                                    std::cout << "Problem reading " << absl::GetFlag(FLAGS_obs_pos_file) << " file" << std::endl;
                                }
                        }
#endif
                    else
                        {
                            std::cout << "Unknown observer position file format. Please use .csv or .nmea files." << std::endl;
                        }
                }
            else
                {
                    // static mode
                    double llh[3];
                    int n_points;
#if USE_GLOG_AND_GFLAGS
                    sscanf(FLAGS_static_position.c_str(), "%lf,%lf,%lf,%d", &llh[0], &llh[1], &llh[2], &n_points);
#else
                    sscanf(absl::GetFlag(FLAGS_static_position).c_str(), "%lf,%lf,%lf,%d", &llh[0], &llh[1], &llh[2], &n_points);
#endif
                    sim_ctl->setstaticposition(llh[0], llh[1], llh[2], n_points);
                    std::cout << " GNSS SIM will run in static observer mode with Lat=" << llh[0] << " Long=" << llh[1] << " Height=" << llh[2] << " and " << n_points << " observation epochs" << std::endl;
                }

#if USE_GLOG_AND_GFLAGS
            // Output signal file
            if (sim_ctl->open_out_file(FLAGS_sig_out_file) == false)
                {
                    std::cout << "Problem creating signal output file " << FLAGS_sig_out_file << std::endl;
                }

            // Ephemeris file

            if (sim_ctl->read_eph(FLAGS_rinex_nav_file) == false)
                {
                    std::cout << "Problem reading RINEX epehemris file " << FLAGS_rinex_nav_file << std::endl;
                }

            if (sim_ctl->setsamplingfreq(FLAGS_sampling_freq) == false)
                {
                    std::cout << "Sampling frequency not valid!" << std::endl;
                }

            if (sim_ctl->open_rinex_obs_file(FLAGS_rinex_obs_file) == false)
                {
                    std::cout << "Problem creating signal RINEX Observation output file " << std::endl;
                }

            // Set CN0
            sim_ctl->setCN0(FLAGS_CN0_dBHz);
#else
            // Output signal file
            if (sim_ctl->open_out_file(absl::GetFlag(FLAGS_sig_out_file)) == false)
                {
                    std::cout << "Problem creating signal output file " << absl::GetFlag(FLAGS_sig_out_file) << std::endl;
                }

            // Ephemeris file

            if (sim_ctl->read_eph(absl::GetFlag(FLAGS_rinex_nav_file)) == false)
                {
                    std::cout << "Problem reading RINEX epehemris file " << absl::GetFlag(FLAGS_rinex_nav_file) << std::endl;
                }

            if (sim_ctl->setsamplingfreq(absl::GetFlag(FLAGS_sampling_freq)) == false)
                {
                    std::cout << "Sampling frequency not valid!" << std::endl;
                }

            if (sim_ctl->open_rinex_obs_file(absl::GetFlag(FLAGS_rinex_obs_file)) == false)
                {
                    std::cout << "Problem creating signal RINEX Observation output file " << std::endl;
                }

            // Set CN0
            sim_ctl->setCN0(absl::GetFlag(FLAGS_CN0_dBHz));
#endif
            std::cout << "Running simulator..." << std::endl;

            sim_ctl->run();
        }
    catch (boost::exception& e)
        {
            LOG(FATAL) << "Boost exception: " << boost::diagnostic_information(e);
        }
    catch (std::exception const& ex)
        {
            LOG(FATAL) << "STD exception: " << ex.what();
        }
    catch (...)
        {
            LOG(INFO) << "Unexpected catch";
        }


    // report the elapsed time
    gettimeofday(&tv, nullptr);
    long long int end = tv.tv_sec * 1000000 + tv.tv_usec;
    std::cout << std::endl;
    std::cout << "Total GNSS-SIM run time "
              << (static_cast<double>(end - begin)) / 1000000.0
              << " [seconds]" << std::endl;

#if USE_GLOG_AND_GFLAGS
    gflags::ShutDownCommandLineFlags();
#else
    absl::FlushLogSinks();
#endif
    std::cout << "GNSS-SIM program ended." << std::endl;
    return 0;
}
